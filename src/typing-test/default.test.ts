import {
  ExactType,
  SubType,
  SuperType,
  Unrelated,
  expectType,
} from "ts-typetools/lib";

import { DeepPick, DeepPickPath } from "../lib";
import { Opaque } from "../lib/Opaque";

expectType<DeepPick<{ a: 0; b: 0 }, "a">>().assert<{ a: 0 }>().toBe(ExactType);
expectType<DeepPick<{ a: 0; b: 0 }, "b">>().assert<{ b: 0 }>().toBe(ExactType);

expectType<DeepPick<{ a: 0; b: 0 }, "!a">>().assert<{ b: 0 }>().toBe(ExactType);
expectType<DeepPick<{ a: 0; b: 0 }, "!b">>().assert<{ a: 0 }>().toBe(ExactType);

expectType<DeepPick<{ a: 0; b: 0 }, "!b" | "!a">>()
  .assert<{}>()
  .toBe(ExactType);
expectType<DeepPick<{ a: 0; b: 0 }, "!b" | "a">>()
  .assert<{ a: 0 }>()
  .toBe(ExactType);
expectType<DeepPick<{ a: 0; b: 0 }, "b" | "!a">>()
  .assert<{ b: 0 }>()
  .toBe(ExactType);

expectType<DeepPick<{ a: 0; b: 0 }, "*">>()
  .assert<{ a: 0; b: 0 }>()
  .toBe(ExactType);

expectType<DeepPick<{ a: { b: 0 } }, "a">>()
  .assert<{ a: { b: 0 } }>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 }; b: 0 }, "a.c">>()
  .assert<{ a: { c: 0 } }>()
  .toBe(ExactType);

expectType<DeepPick<{ a: { b: 0; c: 0 } }, "a.!b">>()
  .assert<{ a: { c: 0 } }>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 } }, "a.!b" | "!a">>()
  .assert<{}>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 } }[], "[].a.!b">>()
  .assert<{ a: { c: 0 } }[]>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 }[] }, "a.[].b">>()
  .assert<{ a: { b: 0 }[] }>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 }[][] }, "a.[].[].!c">>()
  .assert<{ a: { b: 0 }[][] }>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 }; d: { e: 0; f: 0 } }, "~d.!e">>()
  .assert<{ a: { b: 0; c: 0 }; d: { f: 0 } }>()
  .toBe(ExactType);
expectType<
  DeepPick<{ a: { b: 0; c: 0 }; d: { e: 0; f: { g: 0; h: 0 } } }, "~a.!c">
>()
  .assert<{ a: { b: 0 }; d: { e: 0; f: { g: 0; h: 0 } } }>()
  .toBe(ExactType);
expectType<
  DeepPick<
    { a: { b: 0; c: 0 }; d: { e: 0; f: { g: 0; h: 0 } } },
    "~d.~f.g" | "~a.!c"
  >
>()
  .assert<{ a: { b: 0 }; d: { e: 0; f: { g: 0 } } }>()
  .toBe(ExactType);

// opaque modifiers
{
  expectType<
    DeepPickPath<{ a: { b: 0; c: 0 }; d: { e: 0; f: { g: 0; h: 0 } } }>
  >()
    .assert<"d.e" | "d.f" | "d.f.g">()
    .toBe(SubType);

  expectType<
    DeepPickPath<{ a: { b: 0; c: 0 }; d: { e: 0; f: { g: 0; h: 0 } } & Opaque }>
  >()
    .assert<"d.e" | "d.f" | "d.f.g">()
    .toBe(Unrelated);

  expectType<
    DeepPickPath<{ a: { b: 0; c: 0 }; d: { e: 0; f: { g: 0; h: 0 } } } & Opaque>
  >()
    .assert<"*">()
    .toBe(ExactType);
}
