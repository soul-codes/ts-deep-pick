import { Primitive, UnionKeyOf } from "ts-typetools";

import { DeepPickGrammar, DefaultGrammar } from "./DeepPickGrammar";
import { Opaque } from "./Opaque";

export type DeepMutatePath<
  T,
  G extends Pick<DeepPickGrammar, "array" | "prop" | "glob"> = DefaultGrammar
> =
  | (T extends Opaque
      ? never
      : T extends Primitive
      ? never
      : T extends Array<infer T>
      ? T extends Primitive
        ? never
        : InnerKey<T, G["array"], G>
      : { [key in KeyOf<T>]: key | InnerKey<T[key], key, G> }[KeyOf<T>])
  | G["glob"];

type InnerKey<
  T,
  key extends string,
  G extends Pick<DeepPickGrammar, "array" | "prop">
> = T extends Opaque
  ? never
  : T extends Primitive
  ? never
  : T extends Array<infer T>
  ?
      | (T extends Primitive
          ? never
          : `${key}${G["prop"]}${InnerKey<T, G["array"], G>}`)
      | `${key}${G["prop"]}${G["array"]}`
  : {
      [key2 in KeyOf<T>]:
        | `${key}${G["prop"]}${key2}`
        | `${key}${G["prop"]}${InnerKey<T[key2], key2, G>}`;
    }[KeyOf<T>];

type KeyOf<T> = UnionKeyOf<T> & string;
