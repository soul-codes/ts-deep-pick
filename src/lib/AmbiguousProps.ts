import { DeepPickGrammar, DefaultGrammar } from "./DeepPickGrammar";

export type AmbiguousProps<G extends DeepPickGrammar = DefaultGrammar> =
  | G["glob"]
  | `${G["omit"]}${string}`
  | `${G["mutate"]}${string}`
  | `${string}${G["prop"]}${string}`;
