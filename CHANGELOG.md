## [0.2.1](https://gitlab.com/soul-codes/ts-deep-pick/compare/0.2.1-beta.2...0.2.1) (2021-06-07)

## [0.2.1-beta.2](https://gitlab.com/soul-codes/ts-deep-pick/compare/0.2.1-beta.1...0.2.1-beta.2) (2021-06-04)

## [0.2.1-beta.1](https://gitlab.com/soul-codes/ts-deep-pick/compare/0.2.1-beta.0...0.2.1-beta.1) (2021-06-04)


### Bug Fixes

* 🐛 pass test on nested key pick ([8a227e6](https://gitlab.com/soul-codes/ts-deep-pick/commit/8a227e67696077e50d56584c34b7c0e1cfac10be)), closes [#2](https://gitlab.com/soul-codes/ts-deep-pick/issues/2)

## [0.2.1-beta.0](https://gitlab.com/soul-codes/ts-deep-pick/compare/0.2.0...0.2.1-beta.0) (2021-06-04)


### Bug Fixes

* 🐛 make ts-typetools a true dependency ([0a868f9](https://gitlab.com/soul-codes/ts-deep-pick/commit/0a868f98d518d913b9bcfaa739e1cac048bf21f8))

# [0.2.0](https://gitlab.com/soul-codes/ts-deep-pick/compare/0.2.0-beta.1...0.2.0) (2020-12-15)

# [0.2.0-beta.1](https://gitlab.com/soul-codes/ts-deep-pick/compare/0.2.0-beta.0...0.2.0-beta.1) (2020-12-14)


### Bug Fixes

* 🐛 fix incorrect opaque keys, array deep-mutate paths ([3fc7868](https://gitlab.com/soul-codes/ts-deep-pick/commit/3fc7868e324dd8dcbbdb6e7ad236739ea318b535))

# [0.2.0-beta.0](https://gitlab.com/soul-codes/ts-deep-pick/compare/0.1.2...0.2.0-beta.0) (2020-12-13)


### Bug Fixes

* 🐛 use windows compatible rimraf ([ceb40c0](https://gitlab.com/soul-codes/ts-deep-pick/commit/ceb40c0fed0ced460f705cded8381df506fd270d))


### Features

* 🎸 deep mutate types ([3b9d663](https://gitlab.com/soul-codes/ts-deep-pick/commit/3b9d6638c8e620a08eef38085e61de8ce830ae1c))
* 🎸 opaque modifier ([4ff875c](https://gitlab.com/soul-codes/ts-deep-pick/commit/4ff875c573447f884a04a9d0801d141f91d18f5b))

## [0.1.2](https://gitlab.com/soul-codes/ts-deep-pick/compare/0.1.1...0.1.2) (2020-12-04)

## [0.1.1](https://gitlab.com/soul-codes/ts-deep-pick/compare/0.1.0...0.1.1) (2020-12-04)


### Bug Fixes

* 🐛 AmbiguousProps: add missing default grammar type param ([2969ab7](https://gitlab.com/soul-codes/ts-deep-pick/commit/2969ab77745119f5a58952f2a63cc16bf47d936a))

# 0.1.0 (2020-12-04)

