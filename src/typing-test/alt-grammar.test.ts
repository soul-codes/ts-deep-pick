import { ExactType, expectType } from "ts-typetools/lib";

import { DeepPick } from "../lib";

interface G {
  array: "()";
  omit: "^";
  mutate: "?";
  glob: "**";
  prop: "::";
}

expectType<DeepPick<{ a: 0; b: 0 }, "a", G>>()
  .assert<{ a: 0 }>()
  .toBe(ExactType);

expectType<DeepPick<{ a: 0; b: 0 }, "^a", G>>()
  .assert<{ b: 0 }>()
  .toBe(ExactType);

expectType<DeepPick<{ a: 0; b: 0 }, "**", G>>()
  .assert<{ a: 0; b: 0 }>()
  .toBe(ExactType);

expectType<DeepPick<{ a: { b: 0; c: 0 } }, "a::c", G>>()
  .assert<{ a: { c: 0 } }>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 } }, "a::^b", G>>()
  .assert<{ a: { c: 0 } }>()
  .toBe(ExactType);

expectType<DeepPick<{ a: { b: 0; c: 0 } }[], "()::a::^b", G>>()
  .assert<{ a: { c: 0 } }[]>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 }[] }, "a::()::b", G>>()
  .assert<{ a: { b: 0 }[] }>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 }[][] }, "a::()::()::^c", G>>()
  .assert<{ a: { b: 0 }[][] }>()
  .toBe(ExactType);
expectType<DeepPick<{ a: { b: 0; c: 0 }; d: { e: 0; f: 0 } }, "d::^e", G>>()
  .assert<{ d: { f: 0 } }>()
  .toBe(ExactType);

expectType<
  DeepPick<{ a: { b: 0; c: 0 }; d: { e: 0; f: { g: 0; h: 0 } } }, "?a::^c", G>
>()
  .assert<{ a: { b: 0 }; d: { e: 0; f: { g: 0; h: 0 } } }>()
  .toBe(ExactType);
expectType<
  DeepPick<
    { a: { b: 0; c: 0 }; d: { e: 0; f: { g: 0; h: 0 } } },
    "?d::?f::g" | "?a::^c",
    G
  >
>()
  .assert<{ a: { b: 0 }; d: { e: 0; f: { g: 0 } } }>()
  .toBe(ExactType);
